package com.androidosday.androidosfirebase.firebaseandroidos;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("aula");

        Aulas a = new Aulas();
        a.setLocal("123");
        a.setProfessor("12");


        myRef.child("users").child("2").setValue(a);
    }
}
