package br.com.uther.httpcliente_okhttp;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;

import br.com.uther.httpcliente_okhttp.core.AuthController;
import br.com.uther.httpcliente_okhttp.core.Constants;
import br.com.uther.httpcliente_okhttp.model.AuthRequest;
import br.com.uther.httpcliente_okhttp.model.User;

public class OkHttpPostActivity extends AppCompatActivity {

    private String responseJSON;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ok_http_post);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new ExampleOkHttp().execute();
            }
        });
    }

    public String parseUserToJSON(){
        User user = new User();
        user.setLogin("user123");
        user.setSenha("123senha");

        //código que faz o trabalho ;-)
        Gson gson = new Gson();
        String userJSONString = gson.toJson(user);

        AuthRequest r = new AuthRequest();
        r.setJson(userJSONString);

        userJSONString = gson.toJson(r);

        return userJSONString;
    }


    private class ExampleOkHttp extends AsyncTask<Void, Void, Response> {

        OkHttpClient client;
        Request request;

        public final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            client = new OkHttpClient();
            RequestBody body = RequestBody.create(JSON, parseUserToJSON());
            //URL exemplo: n recebe post requests
            request = new Request.Builder()
                    .url(Constants.URL_SERVICE4)
                    .post(body)
                    .build();
        }
        @Override
        protected Response doInBackground(Void... params) {
            try {
                Response response = client.newCall(request).execute();
                return response;
            }catch (IOException e){
                return null;
            }
        }
        @Override
        protected void onPostExecute(Response response) {
            try {
                if (response.message().equals("OK")){
                    responseJSON = AuthController.getInstance().loadAuthFromJSON(response.body().string());
                }
            }catch (IOException e){
                responseJSON = "OPS - Fail connection";
            }
            Toast.makeText(OkHttpPostActivity.this, responseJSON,Toast.LENGTH_LONG).show();
        }
    }

}
