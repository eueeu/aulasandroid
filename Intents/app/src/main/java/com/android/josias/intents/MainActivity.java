package com.android.josias.intents;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Toast;

import com.android.josias.intents.core.ControllerMainActivity;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void openBrowser(View v){
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com"));
        startActivity(i);
    }

    public void nextScreen(View v){
        Intent i = new Intent(this, SecondActivity.class);
        startActivity(i);
        //Não vai voltar para a MainActivity :)
        //finish();
    }

    public void nextDataTransf(View v){
        Intent i = new Intent(this, SecondActivity.class);
        i.putExtra("tagName", "AAAAAAAAAAAAAAAAAAAAAAAAAA");
        startActivity(i);
    }

    public void nextDataTransf2(View v){
        ControllerMainActivity.getInstance().setValue("O valor é esse aqui do controller!");

        Intent i = new Intent(this, SecondActivity.class);
        startActivity(i);
    }

    public void discagem(View v){
        Intent i = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:8388884444"));
        startActivity(i);
    }

    public void chamada(View v){
        Intent i = new Intent(Intent.ACTION_CALL, Uri.parse("tel:8388884444"));
        startActivity(i);
    }

    public void sendEmail(View v){
        Intent emailIntent = new Intent(
                Intent.ACTION_SENDTO, Uri.fromParts("mailto","josiaspaesjr@gmail.com",null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Assunto email");
        emailIntent.putExtra(Intent.EXTRA_TEXT   , "Corpo do email");
        startActivity(Intent.createChooser(emailIntent, "Enviando email..."));
    }

    public void shareText(View v){
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_TEXT   , "Opa! Blz?");
        startActivity(emailIntent);
    }

    public void openApp(View v){
        Intent i;
        PackageManager manager = getPackageManager();
        try {
            i = manager.getLaunchIntentForPackage("br.com.eukip.smaltis");
            if (i == null)
                throw new PackageManager.NameNotFoundException();
            i.addCategory(Intent.CATEGORY_LAUNCHER);
            startActivity(i);
        } catch (PackageManager.NameNotFoundException e) {
            Toast.makeText(this, "Baixe agora!", Toast.LENGTH_SHORT).show();
            Intent i2 = new Intent(Intent.ACTION_VIEW);
            i2.setData(Uri.parse("https://play.google.com/store/apps/details?id=br.com.eukip.smaltis&hl=pt_BR"));
            startActivity(i2);
        }
    }

}
