package com.example.josiasjr.layouts;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.example.josiasjr.layouts.DrawerLayoutActivity;
import com.example.josiasjr.layouts.FrameLayoutActivity;
import com.example.josiasjr.layouts.LinearLayoutActivity;
import com.example.josiasjr.layouts.R;
import com.example.josiasjr.layouts.RelativeLayoutActivity;
import com.example.josiasjr.layouts.core.ControllerCoordinator;
import com.example.josiasjr.layouts.views.CoordinatorConfigActivity;

public class CoordinatorLayoutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coordinator_layout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    public void openEnterAlways(View v){

        switch (v.getId())
        {
            case R.id.btenterAlways:
                ControllerCoordinator.getInstance().setLayoutConfig(R.layout.activity_coordinator_config1);
                break;
            case R.id.btenterAlwaysCollapsed:
                ControllerCoordinator.getInstance().setLayoutConfig(R.layout.activity_coordinator_config2);
                break;
            case R.id.btToolbarIM:
                ControllerCoordinator.getInstance().setLayoutConfig(R.layout.activity_coordinator_config3);
                break;
            case R.id.btToolbarFAB:
                ControllerCoordinator.getInstance().setLayoutConfig(R.layout.activity_coordinator_config4);
                break;
            case R.id.btToolbarCol:
                ControllerCoordinator.getInstance().setLayoutConfig(R.layout.activity_coordinator_config5);
                break;
        }

        Intent i = new Intent(this, CoordinatorConfigActivity.class);
        startActivity(i);
    }
}
